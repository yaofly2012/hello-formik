# FORM处理痛点：
1. 方便获取值
2. validation（Blur, Change, Submit）
3. 错误展示（Toast, input下面，自定义）
4. formatting
输入时自动格式（手机号，身份证号等），但是获取值时又得去除格式化。
5. masking
初始值进行mask，如果用户修改则采用最新的值，否则采用原始值
6. 提交（提交按钮，软键盘的“完成”）

前提：keeping track of values/errors/visited fields

## 涉及UI部分
1. clear Btn

# What is Formik?
A small group of React components and hooks for building forms in React and React Native（非UI库）
1. Getting values in and out of form state
2. Validation and error messages
3. Handling form submission

- 统一了form处理方式（不管是Input还是自定义的form组件）
- 简单，做的不多，但是做的恰到好处，所以小而灵活。

One of Formik’s core design principles is to help you stay organized
Save time

## Issues
1. 所有的字段为啥都要在`initialValues`提前定义属性
- 内部逻辑？
- 编辑器智能提示

# 搭档
1. Yup

# Hooks
1. useFormik
返回：form state, helper方法


# 模式 & 对称性
1. 需要id, name属性标识表单元素（id和name都必须有吗？不一致呢？）
2. 统一的handleChange, handleBlur事件处理函数
3. initailValues, values, errors, touched对象属性数量和名称一致？

## Level1: 
1. useFormik创建formik，
2. 手动给`<from />`绑定`onSubmit`事件，指定初始值，统一的校验方法
3. 手动给field组件绑定onChange, onBlur事件，手动指定value, name

## Level2:
借助getFieldProps方法获取field标准属性对象，然后利用ES6 spread语法指定。

## Level3: React Context
Formik, Form, Field, ErrroMessage


# touched & handleBlur
1. Formik 利用handleBlur处理函数来跟踪field是否被visited(touched)，即要利用`touched`属性则先绑定`onBlur`事件。


# validation
1. validation function 每次字段change都会被执行

## 功能
1. form-level, field-level校验；
2. 同步，异步校验；
3. 内置支持基于Yup的shema form-level的同步校验。

## form-level validate
1. 返回错误对象

## 触发valdation时机
1. onChange, onBlur, 配置validateOnChange, validateOnBlur
2. 手动触发：validateForm, validateField

## field-level和form-level优先级？
1. 添加了field-level校验就不可以使用

## Issues：
> (values, props /* only available when using withFormik */) => {}

为啥？

## Yup

# field
1. 属性 `getFieldPros`
2. meta `getFieldMeta`
- touched
- error


# 提交表单
## 触发方式
1. handleSubmit
这个一般是作为form的onSubmit的事件处理函数，即由form表单触发Formit表单提交过程
2. submitForm
这个一般是手动触发Formit表单提交过程

## 提交过程
### 1. pre-submit
1. >Touch all fields
此时所有的field的meta.touched都是true。具体干了什么？
2. >Set `isSubmitting` to `true`
3. >Increment `submitCount + 1`
记录提交次数的目的是什么？
### 2. valiation
1. >Set `isValidating` to `true`
2. >Run all field-level validations, validate, and validationSchema asynchronously and deeply merge results
- 即优先级是：先执行filed-level验证函数，然后执行form-level的validate, 然后执行form-level的validationSchema。
错误信息以首次校验失败的为准。
- 过程是异步的，由`isValidating`同步标记这个异步过程；
- deeply merge 生成最终的`errors`对象。
3. 过程2中发生任意错误都会终止提交过程，但不会终止校验过程（因为要校验所有的字段）

### 3. submission
1. 调用提交处理函数
2. >you call setSubmitting(false) in your handler to finish the cycle
提交处理函数里调用`setSubmitting(false)`有什么作用，达到什么目的？


# Formik组件成员
1. Formik context
Formik的核心
- form的状态（values, errors, touched），
- helper方法（）

## connect函数
增加Formit context的消费者

## Formik组件
创建表单的入口组件，把`FormikProps`传给子组件，指定子组件方式：
- 通过`render props`方式
- `Formik.component`属性方式
- `Formik.render`属性方式【Deprecated in 2.x】

1. FormikProps
- dirty 标记表单的field值是否发生变更。

2. FormikErrors
3. FormikTouched
4. FormikBag

3. Values

## Field

`Field`组件自动把输入框和Formik建立关系:
1. 利用name取Formik state的值，赋值给input；
2. 绑定input的onChange, onBlur到Formik的handleChange, handleBlur。

渲染表单元素的，默认是input。控制生成的表单元素方式：
- `Field.as`属性
- children
- `Field.component`属性
-

如何添加自定义的onChange, onBlur?

FieldInputProps: 除了Formik使用的额外属性都传给表单元素组件
FieldMetaProps

## useField()
对外暴露fieldProps, fieldMeta。主要用户自定义输入框。

那问题来了如果想自定义输入框是使用`userField`还是`Field` render props呢？
- `useField`只对外暴露fieldProps, fieldMeta
- `Field`render props的参数更丰富（fieldProps, fieldMeta, formikBag）

# Formik没有做的事情：
1. label，input，errorMsg，clear-input-btn 组合
2. 自定义的onChange, onBlur

# React
1. Hooks
2. React Context