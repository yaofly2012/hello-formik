import React from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const SignupSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, 'Too Short')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short')
        .max(50, 'Too Long!')
        .required('Required'),
    email: Yup.string()
        .email('Invalid email')
        .required('Required')
})

// 同步验证
const validateForm = (values) => {
    console.log('validateForm')
    const errors = {}
    if (!values.email) {
        errors.email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }
    if(!/^[a-zA-Z]+$/.test(values.firstName)) {
        errors.firstName = '请输入正确的firstName'
    }
    return errors;
}

// 异步验证
const validateFromAsync = async (values) => {
    const errors = {}
    await sleep(500);
    if (['admin', 'null', 'god'].includes(values.firstName)) {
        errors.firstName = 'Nice try';
    }
    return errors
}

const validateFirstName = (value) => {
    console.log('validateFirstName')
    let error;
    if (value === 'admin') {
        error = 'Nice try!';
    }
    console.log(value)
    return error;
}

export default () => {
    return (
        <div>
            <h2>附属卡信息</h2>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    email: ''
                }}
                validate={validateForm}
                validationSchema={SignupSchema}
                onSubmit={ values => {
                    console.log(values)
                }}
            >
            {
                ({ errors, touched, validateForm, submitForm }) => (
                    <Form>
                        <Field 
                            validate={validateFirstName}
                            name="firstName" 
                            placeholder="first name"/>
                        <ErrorMessage name="firstName" component="div"/>
                        
                        <Field name="lastName" placeholder="last name"/>
                        <ErrorMessage name="lastName" component="div"/>
                        <Field name="email" placeholder="email"/>
                        <ErrorMessage name="email" component="div"/>
                        {/* <button type="submit">Submit</button> */}
                        
                        <button onClick={() => {
                            submitForm() // 调用submitForm方式
                        }}>Submit</button> 
                    </Form>
                )
            }
            </Formik>
        </div>
    )
}