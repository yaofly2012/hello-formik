import { useFormik } from 'formik'
import * as Yup from 'yup'

const SignupSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required!'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required!'),
    email: Yup.string().email('Invalid email').required('Required')
})

export default function SignUp() {
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
        },
        onSubmit: values => {            
            alert(JSON.stringify(values, null, 2));
        },
        validateOnChange: false,
        validationSchema: SignupSchema
        // validate: values => {
        //     const errors = {};
        //     if (!values.firstName) {
        //         errors.firstName = 'Required';
        //     } else if (values.firstName.length > 15) {
        //         errors.firstName = 'Must be 15 characters or less';
        //     }
        
        //     if (!values.lastName) {
        //         errors.lastName = 'Required';
        //     } else if (values.lastName.length > 20) {
        //         errors.lastName = 'Must be 20 characters or less';
        //     }
        
        //     if (!values.email) {
        //         errors.email = 'Required';
        //     } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        //         errors.email = 'Invalid email address';
        //     }
        //     console.log(errors);
        //     return errors;
        // }
    })

    const firstNameProps = formik.getFieldProps('firstName');

    return (
        <div>
            <h1>经营贷-补充信息</h1>
            <form onSubmit={formik.handleSubmit}>                
                <div>
                    <label htmlFor="firstName">FirstName</label>
                    <input type="text" {...firstNameProps} />
                    {formik.errors.firstName && formik.touched.firstName ? <div>{formik.errors.firstName}</div> : null}
                </div>
                <div>
                    <label htmlFor="lastName">LastName</label>
                    <input 
                        id="lastName"
                        name="lastName"
                        type="lastName"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.lastName}
                    />
                    {formik.errors.lastName && formik.touched.lastName ? <div>{formik.errors.lastName}</div> : null}
                </div>
                <div>
                    <label htmlFor="email">Email Address</label>
                    <input 
                        id="email"
                        name="email"
                        type="email"
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                        value={formik.values.email}
                    />
                    {formik.errors.email && formik.touched.email ? <div>{formik.errors.email}</div> : null}
                </div>

                <button type="submit">Submit</button>
            </form>
        </div>
    )
}