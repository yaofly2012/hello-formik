import * as React from 'react'
import { Field, isFunction } from 'formik'
import Input from '../Input'

export default ({name, validate, children, ...restFieldProps}) => {

    return (
        <Field name={name} validate={validate}>
        {
            (props) => { 
                const enhanceProps = {...props, ...restFieldProps};
                return isFunction(children)
                    ? children(enhanceProps) 
                    : <Input {...enhanceProps}/>
            }
        }
        </Field>
    )
}