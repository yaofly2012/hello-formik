import * as React from 'react'
import ErrorMessage from '../ErrorMessage'
import Field from '../Field'
import Label from '../Label'

export default function FieldWithLabel(props)  {
    const { label, disableErrorMsg, children, field, ...restProps } = props;
    
    return (
        <div className="ccard-row">         
            <Label>{label}</Label>   
            <div className="ccard-field-value">
                {
                    !!children 
                    ? children(restProps) 
                    : (
                        <>
                            <Field {...restProps} children={field}/>
                            <ErrorMessage name={props.name} /> 
                        </>
                    )
                }                
            </div>
        </div>
    )
}
