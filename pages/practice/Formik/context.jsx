import { useEffect } from 'react'
import { Formik, useFormikContext, Field } from 'formik'

const AutoSubmitToken = () => {
    const formik = useFormikContext();
    const { values, submitForm } = formik;
    useEffect(() => {
        debugger
        // Submit the form imperatively as an effect as soon as form values.token are 6 digits long
        if (values.token.length === 6) {
            submitForm();
        }
    }, [values, submitForm])

    return null;
}

export default function Context() {
    return (
        <div>
            <h1>useFormikContext</h1>
            <Formik
                initialValues={{
                    token: '',
                    name: ''
                }}
            >
                {
                    (props) => {
                        return (
                            <div>
                                <input 
                                    name="token"
                                    placeholder="token"
                                    value={props.values.token} 
                                    onBlur={props.handleBlur} 
                                    onChange={props.handleChange} />
                                <AutoSubmitToken /> 
                                <Field name="name" placeholder="name" />
                            </div>    
                        )
                    }
                }
            </Formik>
        </div>
    )
}