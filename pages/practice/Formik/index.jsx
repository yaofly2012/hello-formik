import { Formik } from 'formik'

export default class MyFormik extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Formik
                initialValues={{
                    companyName: ''
                }}                
            >
                {
                    (formik) => {
                        debugger
                        return (
                            <div>
                                <input
                                    name="companyName" 
                                    value={formik.values.companyName}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur} 
                                />
                            </div>
                        )
                    }
                }
            </Formik>
        )
    }
}