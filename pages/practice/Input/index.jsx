import { useRef, useCallback, useState } from 'react'
import maskRule from '@ctrip/ccard-util/lib/maskRule'
import { isString } from 'formik'

export default (props) => {
    const { 
        form, 
        field, 
        meta,  
        maskType,
        onBlur: customBlur, 
        onChange: customChange,
        onFocus: customFocus,
        ...restFieldProps 
    } = props;

    const [ focused, setFocused ] = useState(false);
    const changed = useRef(false);
    
    const inputEl = useRef(null);
    const blurValidateTimer = useRef(0);

    const handleClearInput = useCallback(() => {
        form.setFieldValue(field.name, '', false);
        inputEl.current.focus();
        clearTimeout(blurValidateTimer.current);
    }, [form.setFieldValue])

    const handleFocus = (e) => {
        customFocus && customFocus(e);
        setFocused(true);
    }

    const handleBlur = (e) => {
        e.persist();       
        blurValidateTimer.current = setTimeout(() => {                
            form.handleBlur(e);
            customBlur && customBlur(e); // 自定义的
            setFocused(false);
        }, 100)       
    }

    const handleChange = (e) => {       
        changed.current = true; 
        customChange && customChange(e);
        form.handleChange(e);
    }

    const showX = focused && !!meta.value;
    // add mask
    const value = !changed.current 
        && meta.initialValue === meta.value
        && maskRule[maskType] 
        && isString(meta.initialValue) 
        ? maskRule[maskType](meta.value) 
        : meta.value;

    return (
        <div className="ccard-input-control">
            <input 
                ref={ inputEl } 
                className="ccard-input"
                {...field}
                {...restFieldProps}
                value={value}
                onFocus={ handleFocus }
                onBlur={ handleBlur }
                onChange={ handleChange }/>   
            {
                showX
                && <div className="ccard-input-clear" onClick={handleClearInput}>X</div>
            }    
        </div> 
    )
}