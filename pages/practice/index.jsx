import React from 'react'
import { Form } from 'formik'
import FieldWithLabel from './FieldWithLabel'
import Field from './Field'
import Formik from './Formik'

import IdValidFromSelect from '@ctrip/ccard-ui/lib/applyCardComponents/IdValidSelect/IdValidFrom'
import IdValidToSelect from '@ctrip/ccard-ui/lib/applyCardComponents/IdValidSelect/IdValidTo'
import RelationshipSelect from '@ctrip/ccard-ui/lib/applyCardComponents/EnumListSelect/RelationshipSelect'
import HousePropertySelect from '@ctrip/ccard-ui/lib/applyCardComponents/EnumListSelect/HousePropertySelect'
import MarriageSelect from '@ctrip/ccard-ui/lib/applyCardComponents/EnumListSelect/MarriageSelect'
import EducationSelect from '@ctrip/ccard-ui/lib/applyCardComponents/EnumListSelect/EducationSelect'
import CareerSelect from '@ctrip/ccard-ui/lib/applyCardComponents/EnumListSelect/CareerSelect'
import LiveOrWorkCitySelect from '@ctrip/ccard-ui/lib/applyCardComponents/LiveOrWorkCitySelect'
import isEmail from '@ctrip/ccard-util/lib/validate/isEmail'

const bankType = 'pingan'
const cardType = 'XCV1'

export default () => {
    return (
        <div>
            <h2>附属卡信息</h2>
            <Formik
                initialValues={{
                    name: '',
                    firstName: '',
                    lastName: '',
                    email: 'q_yao@ctrip.com',
                    marriage: '',
                    relationship: ''
                }}
                onSubmit={ values => {
                    console.log(values)
                }}
                validateOnChange={!false}
            >
            {
                ({ errors, touched, validateForm }) => (
                    <Form>
                        <FieldWithLabel name="name" 
                            label="姓名"
                            maxLength="12"
                            validate= {
                                (value) => {
                                    if(!value) {
                                        return 'Required'
                                    }
                                    if(!/^[a-zA-Z]+$/.test(value)) {
                                        return '请输入正确的姓名'
                                    }
                                }
                            }
                            placeholder="中文姓名"
                            onChange={ e => {
                                console.log(`custom onChange: "${e.target.value}"`)
                            }}
                        />
                        <FieldWithLabel
                            label="姓名拼音姓名拼音"
                        >
                        {
                            () => {
                                return (
                                    <>
                                    <div className="ccard-field-group">
                                        <Field 
                                            name="firstName" 
                                            label="拼音姓名"
                                            validate= {
                                                (value) => {
                                                    if(!value) {
                                                        return 'Required'
                                                    }
                                                    if(!/^[a-zA-Z]+$/.test(value)) {
                                                        return '请输入正确的拼音名'
                                                    }
                                                }
                                            }
                                            placeholder="first name"/>
                                        <Field name="lastName"
                                            validate= {
                                                (value) => {
                                                    if(!value) {
                                                        return 'Required'
                                                    }
                                                    if(!/^[a-zA-Z]+$/.test(value)) {
                                                        return '请输入正确的拼音姓'
                                                    }
                                                }
                                            }
                                            placeholder="last name"/>
                                    </div>
                                    {
                                        touched.firstName && errors.firstName && <p>{errors.firstName}</p> 
                                        || touched.lastName && errors.lastName && <p>{errors.lastName}</p> 
                                    }
                                    </>
                                )
                            }
                        }
                        </FieldWithLabel>                        
                        <FieldWithLabel
                            name="email" 
                            label="邮箱" 
                            placeholder="email"
                            maskType="email"
                            validate= {
                                (value) => {
                                    if(!value) {
                                        return {
                                            msg: 'Required',
                                            type: 'required',
                                            action: ({isSubmitting}) => {
                                                isSubmitting && alert('请输入邮箱')
                                            }
                                        }
                                    }
                                    if(!isEmail(value)) {
                                        return '请输入正确的邮箱'
                                    }
                                }
                            }
                            onChange={(e) => {
                                console.log(`Email custom onChange ${e.target.value}`)
                            }}/>
                        <FieldWithLabel name="marriage" 
                            label="婚姻状况" 
                            validate={value => {
                                if(!value) {
                                    return 'required'
                                }
                            }}
                            field={({field, form, meta}) => (
                                <MarriageSelect 
                                    value={field.value}
                                    name={meta.name}                                        
                                    bankType={ bankType }
                                    cardType= { cardType }
                                    onSelect={ (value) => {
                                        debugger
                                        form.setFieldValue(field.name, {
                                            code: value.code,
                                            text: value.text
                                        })
                                    }}
                                    
                                />
                            )}
                        />
                        <FieldWithLabel name="relationship" 
                            label={
                                () => {
                                    return <span onClick={ () => {
                                        alert('最好是你媳妇')
                                    }}>联系人关系？</span>
                                }
                            }
                            validate={value => {
                                if(!value) {
                                    return 'required'
                                }
                            }}
                            field={({field, form, meta}) => (
                                <RelationshipSelect 
                                    value={field.value}
                                    bankType={ bankType }
                                    cardType= { cardType }
                                    className="ccard-form-item__relationship"
                                    onSelect={ (value) => {
                                        form.setFieldValue(field.name, {
                                            code: value.code,
                                            text: value.text
                                        })
                                    }}
                                />
                            )}
                            />                                                 
                        <button type="submit">Manual Validate</button>
                    </Form>
                )
            }
            </Formik>
            <style jsx>{`
                .ccard-field-group {
                    display: flex;
                }
            `}</style>
        </div>
    )
}