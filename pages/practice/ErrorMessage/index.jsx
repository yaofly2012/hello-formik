import * as React from 'react';
import { connect, getIn, isFunction, isObject } from 'formik'


class ErrorMessageImpl extends React.Component {
  shouldComponentUpdate(props) {
    if (
      getIn(this.props.formik.errors, this.props.name) !==
        getIn(props.formik.errors, this.props.name) ||
      getIn(this.props.formik.touched, this.props.name) !==
        getIn(props.formik.touched, this.props.name) ||
      Object.keys(this.props).length !== Object.keys(props).length
    ) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    let { component, formik, render, children, name, ...rest } = this.props;

    const touch = getIn(formik.touched, name);
    const error = getIn(formik.errors, name);

    /**
     * 自定错误处理
     */
    if(isObject(error) && isFunction(error.action)){
        error.action({
            isSubmitting: formik.isSubmitting
        })
        return null;
    }

    return !!touch && !!error
        ? render
            ? isFunction(render)
                ? render(error)
                : null
            : children
            ? isFunction(children)
                ? children(error)
                : null
            : component
            ? React.createElement(component, rest, error)
            : <div className="ccard-field-tip ccard-field-status--error">{error}</div>
        : null;
    }
}

const ErrorMessage = connect(ErrorMessageImpl);

export default ErrorMessage
