import React from 'react'
import { isFunction } from 'formik';

export default ({ children }) => {
    if(children === false) {
        return null;
    }

    return (
        <label className="ccard-field-label">
        {
           isFunction(children) 
           ? children()
           : children
        }</label>
    )
}