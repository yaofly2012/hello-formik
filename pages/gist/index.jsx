import React from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'

export default function Index() {
    const initialValues = {
        email: '',
        password: ''
    }

    function validate(values) {
        const errors = {};
        if (!values.email) {
           errors.email = 'Required';
        } else if (
           !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
           errors.email = 'Invalid email address';
        }
        
        return errors;
    }

    function handleSubmit(values, { setSubmitting }) {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 400);
    }


    return (
        <div>
            <h1>经营贷-借款</h1>
            <Formik 
                initialValues={initialValues}
                validate={validate}
                onSubmit={handleSubmit}
                >
                {
                    ({
                        isSubmitting,
                    }) => (
                        <Form>
                            <Field type="email" name="email" />
                            <ErrorMessage name="email" component="div" />
                            <Field type="password" name="password" />
                            <ErrorMessage name="password" component="div" />
                            <button type="submit" disabled={isSubmitting}>
                                Submit
                            </button>
                        </Form>
                    )
                }
            </Formik>
        </div>
    )
}

/**
 * Formik 怎么工作的
 */
function Index1() {
    const initialValues = {
        email: '',
        password: ''
    }

    function validate(values) {
        const errors = {};
        if (!values.email) {
           errors.email = 'Required';
        } else if (
           !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
           errors.email = 'Invalid email address';
        }
        
        return errors;
    }

    function handleSubmit(values, { setSubmitting }) {
        setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
        }, 400);
    }


    return (
        <div>
            <h1>经营贷-借款</h1>
            <Formik 
                initialValues={initialValues}
                validate={validate}
                onSubmit={handleSubmit}
                >
                {
                    ({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                        <form onSubmit={handleSubmit}>
                            <input
                                type="email"
                                name="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                            {errors.email && touched.email && errors.email}
                            <input
                                type="password"
                                name="password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password}
                            />
                            {errors.password && touched.password && errors.password}
                            <button type="submit" disabled={isSubmitting}>
                                Submit
                            </button>
                        </form>
                    )
                }
            </Formik>
        </div>
    )
}