import React from 'react'
import { connect, getIn, Formik, Form, Field } from 'formik'
// import ErrorMessage from './errorMessage'

const ErrorMessage = connect(props => {
    const error = getIn(props.formik.errors, props.name);
    const touched = getIn(props.formik.touched, props.name);

    return touched && error ? <p>{error}</p> : null;
})

export default () => {
    return (
        <div>
            <h2>附属卡信息</h2>
            <Formik
                initialValues={{
                    name: ''
                }}
                validate={values => {
                    const errors = {}
                    if(!values.name) {
                        errors.name = 'Required'
                    } else if(/^[a-zA-Z]+$/.test(values.name)) {
                        errors.name = 'Invalid name'
                    }
                    return errors;
                }}
                onSubmit={ values => { console.log(values) }}
            >
            {
                () => (
                    <Form>
                        <Field name="name"></Field>
                        <ErrorMessage name="name"/>
                        <button type="submit">保存</button>
                    </Form>
                )
            }
            </Formik>
        </div>
    )
}
